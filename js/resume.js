$(function(){
	$(".progress-bar").each(function(){
		each_bar_width = $(this).attr('aria-valuenow');
		$(this).width(each_bar_width + '%');
    });
    
    $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
    
    $(document).on("click", ".left-bar a",function(){
        window.scrollTo(0,0);  
        var type = $(this).find("span").attr("id");

        if (type === "pdf-download") {
            download_pdf();
        }
    });

    $(document).on("click", ".type-block .edit_title", function() {
        $(this).siblings(".edit_content").slideToggle();
    });

    function download_pdf () {
        var file_download = new jsPDF();

        html2canvas(document.getElementById("content"), {
            onrendered: function (canvas) {
                var imgData = canvas.toDataURL('image/png');
                var doc = new jsPDF('p', 'px', [canvas.width/1.7, canvas.height]);


                doc.addImage(imgData, 'PNG', 0, 0);
                doc.save('resume.pdf');
            },
            //allowTaint: true,
            //useCORS: true
        });
        
    }

    $(".type-block .edit_title").not(".edit_title:first").trigger("click");
});